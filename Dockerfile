#### IMage: weather:app1 SIZE: 122mb

#FROM node:alpine
#WORKDIR /app
#COPY package*.json /app/
#COPY . /app
#RUN  npm install
#EXPOSE 3000
#CMD ["node", "server.js"]

## MULTI-STAGE BUILD V:1 IMAGE: WEATHER:multi1 SIZE: 120mb
####stage :1

FROM node:alpine AS Base
WORKDIR /app
COPY package*.json /app/

## stage: 2

FROM Base AS Dependecies
RUN npm install

## stage: 3

FROM Base As Release
WORKDIR /app
COPY --from=Base /app /app
COPY --from=Dependecies /app/node_modules/ ./node_modules
COPY . .
EXPOSE 3000
CMD [ "node", "server.js" ]