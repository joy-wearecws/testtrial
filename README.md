

**The goal of the project: 

Create a CICD pipeline that runs through a basic nodejs build, test, create a docker image, and push it to the GitLab docker repository.

Pipeline Steps:

* Build
* Test
* Package docker Image
* Push to Gitlab Docker Repository

*App used: Nodejs Weather-app**

TO use this project you need to follow the steps below assuming you have basic knowledge of git & docker :

* Clone the repo via the option of your choice.
* add to your repo, commit & push via git commands:
* git add, git commit & git push
* Watch the pipeline in effect**






